# Copy-Clip

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

Copy-Clip is a ridiculously simple way to copy links to the clipboard with Greasemonkey/Tampermonkey

How does work?, it's very simple, hold down the 'Alt' key and click on the link and automatically will be copied to the
Clipboard


## Instalation

To install this script you must have installed Greasemonkey or Tampermonkey, depending on your browser, once installed you must go to:

https://greasyfork.org/es/scripts/25406-copy-clip

There you will find a button that will allow you to install it.

## Features

 - Copy link to clipboard


## Contributing

If you want to collaborate, you had a problem or just want to make a suggestion, you can make them from here:

https://gitlab.com/eliluminado/copy_clip/issues


## Licenses

### Icons
Icon made by Retinaicons [http://www.flaticon.com/authors/retinaicons] from http://www.flaticon.com/free-icon/clipboard_249239

### Extension/Script:

Copy-Clip is released under the [MIT License](http://www.opensource.org/licenses/MIT).
