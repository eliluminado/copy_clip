# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

### Changed


## [1.0.0] - 2015-12-03

### Added
 - Basic files: LICENSE, CHANGELOG.md, README.md and .editorconfig
 - Include a package.json for testing purposes
 - Lint code with GitLab CI
